using System;

namespace Program
{
	class Vector3D
	{	
		#region Fields
		
		private double _x;
		private double _y;
		private double _z;
		
		#endregion
		
		#region Constructors
		
		public Vector3D(double x, double y, double z)
		{
			_x = x;
			_y = y;
			_z = z;
		}
		
		#endregion
		
		#region Properties
		
		public double X 
		{ 
			get
			{
				return _x;
			} 
			set
			{
				_x = value;
			} 
		}
		
		public double Y 
		{ 
			get
			{
				return _y;
			}			
			set 
			{
				_y = value;
			}
		}
		
		public double Z 
		{ 
			get
			{
				return _z;
			}			
			set
			{
				_z = value;
			}			
		}
		
		public double Length
		{
			get
			{
				return Math.Sqrt( Math.Pow(_x, 2) + Math.Pow(_y, 2) + Math.Pow(_z, 2) );
			}
		}
		
		#endregion
		
		#region Methods		
		
		public static double TripleProduct(Vector3D vec1, Vector3D vec2, Vector3D vec3)
		{
			return DotProduct(vec1, CrossProduct(vec2, vec3));
		}
		
		public static double DotProduct(Vector3D vec1, Vector3D vec2)
		{
			return vec1.X * vec2.X + vec1.Y * vec2.Y + vec1.Z * vec2.Z;
		}
		
		public static Vector3D CrossProduct(Vector3D vec1, Vector3D vec2)
		{	
			return new Vector3D(vec1.Y * vec2.Z - vec2.Y * vec1.Z, 
							   (vec1.X * vec2.Z - vec2.X * vec1.Z) * -1,
								vec1.X * vec2.Y - vec2.X * vec1.Y);
		}
		
		public static double AngleBetween(Vector3D vec1, Vector3D vec2)
		{
			return Math.Acos(DotProduct(vec1, vec2) / (vec1.Length * vec2.Length)) * (180.0 / Math.PI);
		}
		
		public static bool Equals(Vector3D vec1, Vector3D vec2)
		{
			if(vec1.X == vec2.X && vec1.Y == vec2.Y && vec1.Z == vec2.Z)
			{
				return true;
			}			
			else
			{
				return false;
			}
		}
		
		public bool Equals(Vector3D vec1)
		{
			if(this.X == vec1.X && this.Y == vec1.Y && this.Z == vec1.Z)
			{
				return true;
			}			
			else
			{
				return false;
			}
		}
		
		public override string ToString()
		{
			return String.Format("({0}, {1}, {2})", _x, _y, _z);
		}
		
		#endregion
	
		#region Operators
		
		public static Vector3D operator +(Vector3D vec1, Vector3D vec2)
		{
			return new Vector3D(vec1.X + vec2.X, vec1.Y + vec2.Y, vec1.Z + vec2.Z);
		}
		
		public static Vector3D operator -(Vector3D vec1, Vector3D vec2)
		{
			return new Vector3D(vec1.X - vec2.X, vec1.Y - vec2.Y, vec1.Z - vec2.Z);
		}
		
		#endregion
	}
	
	class VectorTest
	{
		static void Main()
		{
			Vector3D vec1 = new Vector3D(3, 5, 2);
			Vector3D vec2 = new Vector3D(1, 7, 1);		
			Vector3D vec3 = new Vector3D(2, 6, 3);
						
			Console.WriteLine("Vector1 lenght is: {0}", vec1.Length);
			
			Console.WriteLine("Triple product is: {0}", Vector3D.TripleProduct(vec1, vec2, vec3));
			
			Console.WriteLine("Angle between vector1 and vector2: {0}", Vector3D.AngleBetween(vec1, vec2));
			
			Console.WriteLine("Dot product vector1 and vector2: {0}", Vector3D.DotProduct(vec1, vec2));
			
			vec3 =  Vector3D.CrossProduct(vec1, vec2);
			Console.WriteLine("CrossProduct product vector1 and vector2: {0}", vec3.ToString());
			
			Console.WriteLine("Is vector1 equal vector2?: {0}", Vector3D.Equals(vec1, vec2));
			
			vec3 = vec1 + vec2;
			Console.WriteLine("Vector1 + vetor2: {0}", vec3.ToString());
			
			vec3 = vec1 - vec2;
			Console.WriteLine("Vector1 - vetor2: {0}", vec3.ToString());
			
			Console.ReadKey();
		}
	}	
}